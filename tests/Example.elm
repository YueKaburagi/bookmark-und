module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import Result exposing (Result(..))
import Bookmark.Parser exposing (parser)
import Parser.Common exposing (many, some, some_, whitespace, whitespace1)
import Bookmark.Types exposing (TagQuery(..))
import Parser exposing (run, succeed, (|.), (|=))
import Array as Array

suite : Test
suite =
  describe "parser test"
    [ describe "simple"
        [ test "plain text" <|
          \_ -> testParse "abc" <| AnyText "abc"
        , test "plain text strict" <|
          \_ -> Expect.equal (Ok (AnyTextStrict "def")) (run parser "def;")
        , test "tag" <|
          \_ -> Expect.equal (Ok (Tag "tag")) (run parser "t:tag")
        , test "tag strict" <|
          \_ -> Expect.equal (Ok (TagStrict "hoge")) (run parser "t:hoge;")
        , test "url" <|
          \_ -> testParse "u:fuga" <| Url "fuga"
        ]
    , describe "recursive"
        [ test "not" <|
          \_ -> testParse "!t:piyo" <| Not (Tag "piyo")
        , test "any" <|
          \_ -> testParse "(| t:abc; src )" <| Any (Array.fromList [TagStrict "abc", AnyText "src"])
        , test "all" <|
          \_ -> testParse "(& t:abc;  src )" <| All (Array.fromList [TagStrict "abc", AnyText "src"])
        ]
    , describe "extra"
        [ test "string starts )" <|
          \_ -> Expect.err (run parser ")s")
        , test "with \" " <|
          \_ -> testParse "\"ab cd\"" <| AnyText "ab cd"
        ]
    , describe "unit"
      [ describe "whitespace1"
          [ test "one space" <|
            \_ -> Expect.ok (run whitespace1 " ")
          , test "two or more" <|
            \_ -> Expect.ok (run whitespace1 "    ")
          , test "before text" <|
            \_ -> Expect.ok (run (succeed identity |. whitespace1 |= parser) "   abc")
          , test "between text" <|
            \_ -> Expect.ok (run (succeed identity |. whitespace1 |= parser |. whitespace1) "   abc   ")
          , test "various space characters" <|
            \_ -> Expect.ok (run whitespace1 "  \t\t\r\n\u{00a0} \u{200b} \u{3000}")
          ]
      , describe "many"
          [ test "zero" <|
            \_ -> Expect.ok (run (many whitespace1) "")
          ]
      ]
    ]





testParse : String -> TagQuery -> Expectation
testParse s tq = Expect.equal (Ok tq) (run parser s)
