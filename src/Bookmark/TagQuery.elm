module Bookmark.TagQuery exposing ( condition 
                                  , tagFilter
                                  , show
                                  , pushQuery
                                  , preventation
                                  , sort
                                  )

import Array exposing (Array)
import Bookmark.Types exposing (Article, TagQuery(..))
import Parser.Common exposing (isWhite)
import Maybe.Extra as Maybe
import Set exposing (Set)


any : (a -> Bool) -> Array a -> Bool
any f = Array.foldr (\a s -> s || f a) False

        
all : (a -> Bool) -> Array a -> Bool
all f = Array.foldr (\a s -> s && f a) True


sany : (a -> Bool) -> Set a -> Bool
sany f = Set.foldr (\a s -> s || f a) False


aall : (a -> Bool) -> Set a -> Bool
aall f = Set.foldr (\a s -> s && f a) True


preventation : TagQuery -> Bool
preventation tagquery =
  case tagquery of
    Url str ->
      2 >= String.length str

    AnyText str ->
      1 >= String.length str

    Tag str ->
      2 >= String.length str

    _ ->
      False


-- unicodeの平仮名領域の 3041-3094 と 片仮名領域の 30A1-30F4 は同じ順で並んでいる
katakanaAsHiragana : Char -> Char
katakanaAsHiragana c =
  if '\u{30A1}' <= c && c <= '\u{30F4}'
  then (Char.fromCode << (\x -> x - 0x60) << Char.toCode) c
  else c


expandTarget : String -> String
expandTarget = String.map (Char.toLower << katakanaAsHiragana)


contains : String -> String -> Bool
contains key target = String.contains key target || String.contains key (expandTarget target)


containsN : Int -> String -> String -> Bool
containsN limit key target =
  (String.length target < limit) && contains key target


containsV : String -> String -> Bool
containsV key target = containsN (String.length key + 5) key target




-- Order as pirority. prior LT
comparator : TagQuery -> TagQuery -> Order
comparator t1 t2 =
  case (t1,t2) of
    (Any _, _) -> GT

    (All _, _) -> LT

    (_, Any _) -> LT

    (_, All _) -> GT

    _ -> EQ


descent : (a -> a -> Order) -> a -> a -> Order
descent f a b =
  case f a b of
    LT -> GT

    GT -> LT

    EQ -> EQ


onArray : (List a -> List b) -> Array a -> Array b
onArray f = Array.fromList << f << Array.toList


sort : TagQuery -> TagQuery
sort tagquery =
  case tagquery of
    All t -> 
      All <| onArray (List.sortWith (descent comparator)) <| Array.map sort t

    Any t ->
      Any <| onArray (List.sortWith (descent comparator)) <| Array.map sort t

    Not t ->
      Not (sort t)

    t ->
      t



condition : TagQuery -> Article -> Bool
condition tagquery a =
  let
    strict = (==)
    partial = contains
  in
  case tagquery of
    Tag str ->
      sany (partial str) a.tags || any (\r -> sany (partial str) r.tags) a.records

    TagStrict str ->
      sany (strict str) a.tags || any (\r -> sany (strict str) r.tags) a.records

    Url str ->
      any (\r -> contains str r.url) a.records

    AnyText str ->
      contains str a.title || any (\r -> contains str r.name) a.records

    AnyTextStrict str ->
      strict str a.title || any (\r -> strict str r.name) a.records

    Not t ->
      not (condition t a)

    All l ->
      all (\t -> condition t a) l

    Any l ->
      any (\t -> condition t a) l


tagFilter : TagQuery -> Set String -> Set String
tagFilter tagquery =
  case tagquery of
    TagStrict str ->
      Set.filter ((/=) str)

    Not _ ->
      identity

    All l ->
      \tags -> Array.foldr tagFilter tags l

    Any _ ->
      identity

    _ ->
      identity


show : TagQuery -> String
show tagquery =
  case tagquery of
    Tag str ->
      "t:" ++ normalizeStr str

    TagStrict str ->
      "t:" ++ normalizeStr str ++ ";"

    Url str ->
      "u:" ++ normalizeStr str

    AnyText str ->
      normalizeStr str

    AnyTextStrict str ->
      normalizeStr str ++ ";"

    Not t ->
      "!" ++ show t

    All l ->
      Array.foldr (\a s -> s ++ " " ++ show a) "(&" l ++ " )"

    Any l ->
      Array.foldr (\a s -> s ++ " " ++ show a) "(|" l ++ " )"


normalizeStr : String -> String
normalizeStr s =
  if String.any isWhite s then "\"" ++ s ++ "\"" else s

                
pushQuery : TagQuery -> TagQuery -> TagQuery
pushQuery q r =
  case r of
    All qs ->
      All (Array.push q qs)

    ot ->
      All (Array.fromList [q, ot])



              
