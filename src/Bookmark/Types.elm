module Bookmark.Types exposing ( Article
                               , Record
                               , Thumbnail(..)
                               , TagQuery(..)
                               , Priset
                               -- util
                               , articleTempId
                               , extractRate
                               )


import Array exposing (Array, toList)
import Array.NonEmpty exposing (NonEmptyArray)
import Maybe exposing (Maybe)
import String exposing (append, concat)
import Set exposing (Set)


type Thumbnail
    = SingleImage String
    | MultiImage (NonEmptyArray String)


type alias Record =
    { name: String
    , url: String
    , tags: Set String
    , comment: Maybe String
    , thumbnail: Maybe Thumbnail
    }


type alias Article =
    { title: String
    , records: Array Record
    , tags: Set String
    , comment: Maybe String
    }


articleTempId : Article -> String
articleTempId a = append a.title <| concat <| Set.toList a.tags


extractRate : Article -> String
extractRate a =
  let
    rs = Set.fromList ["+1", "+0.9", "+0.8", "+0.7", "+0.6", "+0.5", "+0.4", "+0.3", "+0.2", "+0.1"]
  in
    Maybe.withDefault "+0.0" <| List.head <| Set.toList <| Set.intersect rs a.tags



type TagQuery
    = Tag String
    | TagStrict String
    | Url String
    | AnyText String
    | AnyTextStrict String
    | Not TagQuery
    | All (Array TagQuery)
    | Any (Array TagQuery)

type alias Priset = { name: String, cond: TagQuery }


