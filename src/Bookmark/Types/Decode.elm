module Bookmark.Types.Decode exposing ( article
                                      , record
                                      , tagQuery, tagQueryAtInput
                                      , priset
                                      )

import Array exposing (Array)
import Array.NonEmpty as NEA
import Json.Decode exposing (field, Decoder)
import Json.Decode as Decode
import Maybe.Extra as Maybe
import Maybe
import Set as Set

import Bookmark.Types exposing (..)


article : Decoder Article
article =
    Decode.map4 Article
        (field "title" asString)
        (Decode.map (Maybe.withDefault Array.empty)
             <| Decode.maybe
             <| field "records"
             <| Decode.array record)
        (field "tags" (Decode.map Set.fromList <| Decode.list asString))
        (Decode.maybe (field "comment" Decode.string))


record : Decoder Record
record =
    Decode.map5 Record
        (field "name" asString)
        (field "url" Decode.string)
        (Decode.map (Maybe.withDefault Set.empty)
             <| Decode.maybe
             <| field "tags"
             <| Decode.map Set.fromList (Decode.list asString))
        (Decode.maybe (field "comment" Decode.string))
        (Decode.map Maybe.join <| Decode.maybe (field "thumbnail" thumbnail))


thumbnail : Decoder (Maybe Thumbnail)
thumbnail =
  Decode.oneOf
    [ Decode.map (Maybe.map MultiImage << NEA.fromArray) <| Decode.array asString
    , Decode.map (Just << SingleImage) asString
    ]


priset : Decoder Priset
priset =
    Decode.map2 Priset
        (field "name" asString)
        (Decode.oneOf
             [ field "pred" tagQuery
             , field "name" tagQuery
             ])


tagQuery : Decoder TagQuery
tagQuery =
    let
        lazy = Decode.lazy (\_ -> tagQuery)
    in
        Decode.oneOf
            [ Decode.map TagStrict asString
            , Decode.map TagStrict (field "tag" asString)
            , Decode.map Not (field "not" lazy)
            , Decode.map All (field "all" (Decode.array lazy))
            , Decode.map Any (field "any" (Decode.array lazy))
            ]


tagQueryAtInput : Decoder TagQuery
tagQueryAtInput =
    let
        lazy = Decode.lazy (\_ -> tagQueryAtInput)
    in
        Decode.oneOf
            [ Decode.map AnyText (field "title" asString)
            , Decode.map Tag (field "tag" asString)
            , Decode.map Url (field "url" asString)
            , Decode.map Not (field "not" lazy)
            , Decode.map All (field "all" (Decode.array lazy))
            , Decode.map Any (field "any" (Decode.array lazy))
            ]



asString : Decoder String
asString =
    Decode.oneOf
        [ Decode.string
        , Decode.map String.fromInt Decode.int
        , Decode.map String.fromFloat Decode.float
        ]
