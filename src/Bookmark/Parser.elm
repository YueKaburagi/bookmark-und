
module Bookmark.Parser exposing (parse, parser)

import Result as Result
import Parser exposing (..)
import List as List
import Array as Array

import Parser.Common exposing (many, whitespace1, isWhite)
import Bookmark.Types exposing (TagQuery(..))


parse : String -> Maybe TagQuery
parse = Result.toMaybe << run parser


parser : Parser TagQuery
parser = atom


colon : Parser ()
colon = oneOf [symbol ":", symbol "："]


atom : Parser TagQuery
atom =
  let
    urlPat =
      succeed Url
        |. symbol "u"
        |. colon
        |= str
    urlPatI =
      succeed Url
        |= backtrackable ncstr
        |. backtrackable colon
        |. symbol "u"
    tagPat =
      succeed Tag
        |. symbol "t"
        |. colon
        |= str
    tagPatS =
      succeed TagStrict
        |. backtrackable (symbol "t")
        |. backtrackable colon
        |= backtrackable str
        |. symbol ";"
    tagPatI =
      succeed Tag
        |= backtrackable ncstr
        |. backtrackable colon
        |. symbol "t"
    tagPatIS =
      succeed TagStrict
        |= backtrackable ncstr
        |. backtrackable colon
        |. backtrackable (symbol "t")
        |. symbol ";"
    anyTextPat =
      succeed AnyText
        |= str
    anyTextPatS =
      succeed AnyTextStrict
        |= backtrackable str
        |. symbol ";"
    notPat =
      succeed Not
        |. symbol "!"
        |= lazy (\_ -> atom)
    anyPat =
      map Any <|
        succeed Array.fromList
          |. backtrackable (symbol "(")
          |. symbol "|"
          |= many (succeed identity |. whitespace1 |= lazy (\_ -> atom))
          |. whitespace1
          |. symbol ")"
    allPat =
      map All <|
        succeed Array.fromList
          |. backtrackable (symbol "(")
          |. symbol "&"
          |= many (succeed identity |. whitespace1 |= lazy (\_ -> atom))
          |. whitespace1
          |. symbol ")"
  in
    oneOf [ notPat
          , urlPat, urlPatI
          , tagPatS, tagPat, tagPatIS, tagPatI
          , anyPat, allPat
          , anyTextPatS, anyTextPat
          ]




str : Parser String
str =
  oneOf [quotedStr, normStr]


ncstr : Parser String
ncstr =
  oneOf [quotedStr, noColonStr]


quotedStr : Parser String
quotedStr =
  succeed identity
    |. symbol "\""
    |= (getChompedString <| chompUntil "\"")
    |. symbol "\""


commonStrParserGen : (Char -> Bool) -> (Char -> Bool) -> Parser String
commonStrParserGen validHead validChar =
  getChompedString <|
    succeed ()
      |. chompIf validHead
      |. chompWhile validChar


normStr : Parser String
normStr =
  let
    validHead c = not <| (isWhite c || c == ';' || c == ')')
    validChar c = not <| (isWhite c || c == ';')
  in
    commonStrParserGen validHead validChar


noColonStr : Parser String
noColonStr =
  let
    validHead c = not <| (isWhite c || c == ';' || c == ')')
    validChar c = not <| (isWhite c || c == ':' || c == '：' || c == ';')
  in
    commonStrParserGen validHead validChar
