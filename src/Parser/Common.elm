
module Parser.Common exposing (many, some, some_, whitespace, whitespace1, isWhite)

import Parser exposing (..)
import Tuple exposing (pair)



{-| These space characters. 
`' ', '\\t', '\\n', '\\r', '\\u00a0', '\\u200b', '\\u3000'`
-}
isWhite : Char -> Bool
isWhite c = List.any ((==) c) [' ', '\t', '\n', '\r', '\u{00a0}', '\u{200b}', '　']


{-| Chomp one or more spaces. -}
whitespace1 : Parser ()
whitespace1 =
  chompIf isWhite |. whitespace

{-| Chomp zero or more spaces. -}
whitespace : Parser ()
whitespace =
  chompWhile isWhite

            
{-| Parse zero or more elements. This function uses argument backtrackably. -}
many : Parser a -> Parser (List a)
many p =
  let
    step : List a -> Parser (Step (List a) (List a))
    step l =
      oneOf 
      [ succeed (\x -> Loop ( x :: l )) |= backtrackable p
      , succeed (Done <| List.reverse l)
      ]
  in
    loop [] step


{-| Parse one or more elements. This function uses argument backtrackably. -}
some : Parser a -> Parser (a, List a)
some = some_ pair

some_ : (a -> List a -> b) -> Parser a -> Parser b
some_ f p =
  succeed f
    |= backtrackable p
    |= many p
