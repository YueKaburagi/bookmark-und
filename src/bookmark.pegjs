

{
  function makeInteger(o) {
    return parseInt(o.join(""), 10);
  }
}

start
  = additive
  / space? p:expr space? { return p; }

additive
  = left:multiplicative "+" right:additive { return left + right; }
  / multiplicative

multiplicative
  = left:primary "*" right:multiplicative { return left * right; }
  / primary

primary
  = integer
  / "(" additive:additive ")" { return additive; }

integer "integer"
  = digits:[0-9]+ { return makeInteger(digits); }



expr
  = all
  / any
  / tag
  / not
  / url
  / title

all
  = "(&" xs:(space expr)+ space? ")" { return {all: xs.map( (l) => l[1] )}; }

any
  = "(|" xs:(space expr)+ space? ")" { return {any: xs.map( (l) => l[1] )}; }

not
  = "!" x:expr { return {not: x}; }

tag
  = "t:" atom:atom { return {tag: atom}; }

title
  = atom:atom  { return {title: atom}; }

url
  = "u:" atom:atom { return {url: atom}; }


atom
  = '"' atom:$([^)"][^"]*) '"' { return atom; }
  / "'" atom:$([^)'][^']*) "'" { return atom; }
  / atom:$([^) \r\n\t][^ \r\n\t]*) { return atom; }

space
  = spc:[ \r\n\t]+