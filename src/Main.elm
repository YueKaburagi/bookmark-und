port module Main exposing (clickAny, keyCtrlL)

import Platform exposing (Program)
import Basics.Extra exposing (uncurry)
import Browser
import Browser.Dom as Dom
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick, onInput, stopPropagationOn)
import Html.Attributes exposing (class)
import Html.Attributes as Attr
import List exposing (length, drop, head)
import Maybe exposing (withDefault, Maybe(..))
import Array exposing (Array)
import Array.NonEmpty as NEA
import Maybe.Extra as Maybe
import Json.Decode exposing (Value, Error, Decoder, decodeValue, field)
import Json.Decode as Decoder
import Result
import Result.Extra as Result
import String as String
import Task exposing (Task)
import Dict as Dict exposing (Dict)
import Dict.Extra as Dict
import Set as Set
import Random as Random
import Random.List as List
import Process


import Bookmark.TagQuery as TagQuery exposing (pushQuery)
import Bookmark.Types exposing (Article, Record, Thumbnail(..), TagQuery, Priset)
import Bookmark.Types as Bookmark
import Bookmark.Types.Decode as Decoder
import Bookmark.TagQuery as Bookmark
import Bookmark.Parser as Parser

main : Program Value Model Msg
main =
  Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }



type alias Model =
    { query: Maybe TagQuery
    , articles: Dict String Article
    , visibles: List Article
    , prisets: List Priset
    , input: String
    , menu: Maybe Menu
    , errLog: List Error
    }
  

type alias Menu =
    { target: String
    , lifecount: Int
    }


init : Value -> (Model, Cmd Msg)
init v =
    let
        pris = decodeValue (field "prisets" <| Decoder.list Decoder.priset) v
        arts = decodeValue (field "data" <| Decoder.list Decoder.article) v
        cumc = Result.unpack List.singleton (always [])
        dip x = (Bookmark.articleTempId x, x)
        orderingPri x = { x | cond = TagQuery.sort x.cond }
    in
        ( { query = Nothing
          , articles = Dict.fromList <| List.map dip (Result.withDefault [] arts)
          , visibles = []
          , prisets = List.map orderingPri <| Result.withDefault [] pris
          , input = ""
          , menu = Nothing
          , errLog = cumc pris ++ cumc arts
          }
        , Cmd.none )


type Msg
    = AddQuery TagQuery
    | SetQuery TagQuery
    | SetMenuTarget String
    | RemoveMenuTarget
    | FireInput String
    | Randomize
    | SortByRate
    | ViewFixed (List Article)
    | StartFilter
    | DelayFilter TagQuery (List Article) (List Article) -- filter待ちのarticles？
    | FocusOn String
    | FocusR (Result Dom.Error ())
    | DoNothing



                        


alwaysDecoder : a -> Decoder (a, Bool)
alwaysDecoder =
    let
        cont x = (x,True)
    in
        Decoder.map cont << Decoder.succeed


drawPriset : Priset -> Html Msg
drawPriset p =
    Html.button [ onClick (SetQuery p.cond) ] [ text p.name ]
      

drawNothing : Html a
drawNothing =
    Html.span [ Attr.style "display" "none" ] []


drawArticle : Article -> Html Msg
drawArticle a =
    Html.article []
        [ Html.div [ class "images" ] ( drawThumbnails <| Array.toList a.records ) 
        , Html.span [ class "title-line" ] [ text a.title ]
        , Html.span [ class "record" ] ( List.map drawRecord (Array.toList a.records) )
        , Html.span [ class "tags" ] ( List.map drawTag (Set.toList a.tags) )
        , Maybe.withDefault drawNothing (Maybe.map drawComment a.comment)
        ]


drawThumbnails : List Record -> List (Html a)
drawThumbnails rs =
  let
    dummyImage = Html.img [ Attr.src "dummy_48x48.png" ] []
    single r = Maybe.withDefault dummyImage <| Maybe.map drawThumbnail r.thumbnail
  in
    List.map single rs


drawThumbnail : Thumbnail -> Html a
drawThumbnail img =
    case img of
      SingleImage t ->
        Html.img [ Attr.src t ] []

      MultiImage ts ->
        Html.div [ class "multiImages" ] (List.map (\x -> Html.img [ Attr.src x ] []) <| NEA.toList ts)


drawRecord : Record -> Html a
drawRecord r =
    Html.a [ Attr.href r.url, Attr.target "_blank" ] [ text r.name ]

        
drawTag : String -> Html Msg
drawTag tag =
    Html.span [ class "tag", stopPropagationOn "click" (alwaysDecoder (SetMenuTarget tag)) ]
        [ text tag ]


drawComment : String -> Html a
drawComment str =
    Html.span [ class "lesser" ] [ text str ]


drawFiltered : Model -> Html Msg
drawFiltered model =
    case model.query of
        Nothing ->
            drawNothing

        Just tq ->
            let
                fixts f a = { a | tags = f a.tags }
            in
                if Bookmark.preventation tq
                then drawNothing
                else div []
                    (List.map
                         (drawArticle << fixts (Bookmark.tagFilter tq))
                         model.visibles)


delayedMakeVisibles : Int -> TagQuery -> List Article -> Task x (List Article, List Article)
delayedMakeVisibles limit tq source =
  let
    next src r =
      if List.length r >= limit
      then Task.succeed (src, List.reverse r)
      else
        case src of
          [] -> Task.succeed (src, List.reverse r)
              
          (x :: xs) ->
            if Bookmark.condition tq x then next xs (x :: r) else next xs r
  in
    Process.sleep 100 |> Task.andThen (\_ -> next source [])
    -- sleep 挟まないと思った通りに async しない
                      

drawErrString : Error -> Html a
drawErrString e =
    Html.pre [] [ text (Decoder.errorToString e) ]


drawTagMenu : String -> Html Msg
drawTagMenu str =
    Html.div [ class "popup-tag-menu" ]
        [ Html.span [ onClick (AddQuery <| Bookmark.TagStrict str) ] [ text ("Add " ++ str) ]
        , Html.span [ onClick (AddQuery <| Bookmark.Not <| Bookmark.TagStrict str) ] [ text ("Add NOT " ++ str) ]
        , Html.span [ onClick (SetQuery <| Bookmark.TagStrict str) ] [ text ("Set " ++ str) ]
        ]


port setSelectAll : String -> Cmd a
port clickAny : (Value -> a) -> Sub a
port keyCtrlL : (Value -> a) -> Sub a


subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.batch
    [ clickAny (always RemoveMenuTarget)
    , keyCtrlL (always <| FocusOn "search" ) -- input#search
    ]


-- |  to use only user click event
fixInput : Model -> Model
fixInput model =
  let
    ns = Maybe.withDefault "" (Maybe.map Bookmark.show model.query)
  in
    { model | input = ns }


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  let
    withNone x = (x, Cmd.none)
    withStartFilter x = (x, Task.perform identity (Task.succeed StartFilter))
    rmMenu x = { x | menu = Nothing }
  in
    case msg of
      DoNothing ->
        withNone model

      AddQuery tq ->
        withStartFilter
        <| rmMenu
        <| fixInput
        <| { model | query = Maybe.orLazy (Maybe.map (pushQuery tq) model.query) (\_ -> Just tq) }

      SetQuery tq ->
        withStartFilter
        <| rmMenu
        <| fixInput ({ model | query = Just tq })

      SetMenuTarget str ->
        withNone
        <| { model | menu = Just { target = str, lifecount = 5 } }

      RemoveMenuTarget ->
        withNone <| rmMenu model

      FireInput str ->
        case Parser.parse str of
             Just tq ->
               withStartFilter
               <| { model | input = str, query = Just tq }

             Nothing ->
               withNone <| { model | input = str }

      Randomize ->
        (model, Random.generate ViewFixed (List.shuffle model.visibles))

      SortByRate ->
        withNone
        <| let
            pd = Dict.groupBy Bookmark.extractRate model.visibles
         in
           { model | visibles = List.concat <| List.reverse <| Dict.values pd }

      StartFilter ->
        case model.query of
          Nothing -> withNone model

          Just tq ->
            ( { model | visibles = [] }
            , let
                list = Dict.values model.articles
              in
                Task.perform identity (Task.succeed <| DelayFilter tq list [])
            )

      DelayFilter tq rest rs ->
        if Just tq == model.query  -- query が変更されてなければ続ける
        then
          ( { model | visibles = model.visibles ++ rs }
          , case rest of
              [] -> Cmd.none
              
              _ ->
                Task.perform (uncurry (DelayFilter tq)) (delayedMakeVisibles 20 tq rest)
          )
        else
          withNone model

      ViewFixed vs ->
        withNone
        <| { model | visibles = vs }

      FocusOn str ->
        (model, Cmd.batch [ setSelectAll "search"
                          , Task.attempt FocusR <| Dom.focus str
                          ])

      FocusR r ->
        case r of
          Err (Dom.NotFound id) ->
            withNone <| { model | input = id }
          Ok () ->
            withNone model
                        



view : Model -> Html Msg
view model =
  div []
    [ div [ class "priset_buttons" ] (List.map drawPriset model.prisets)
    , div []
      [ Html.input
        [ Attr.type_ "text"
        , Attr.autocomplete False
        , Attr.size 128
        , Attr.id "search"
        , Attr.value model.input
        , onInput FireInput  -- for incremental search
        ] []
      , Html.button [ Attr.tabindex (-1), onClick Randomize ] [ text "Random" ]
      , Html.button [ Attr.tabindex (-1), onClick SortByRate ] [ text "ByRate" ]
      ]
    , Html.hr [] []
    , div [ class "open-blank" ]
        [ Html.a [ class "open-blank"
                 , Attr.href "about:blank", Attr.target "_blank", Attr.tabindex (-1) ]
              [ text "---------- open-blank-page ----------" ]
        ]
    , Html.hr [] []
    , Html.span [ class "huge", class "center" ] [ text "BOOKMARKS" ]
    , Html.hr [] []
    , Maybe.withDefault drawNothing (Maybe.map (drawTagMenu << .target) model.menu)
    , div [] (List.map drawErrString model.errLog)
    , Html.span [ class "lesser" ] [ text " +1 手放しで好き // +0.9 好きになりきれない好き // +0.8 私はこれ好き // +0.7 ひとを選ぶだろうけど好き // +0.5 たぶん好き // +0.3 わかんない // +0.1 なんか気になる" ]
    , Html.section [ Attr.id "autoins" ] [ drawFiltered model ]
    , Html.span [ class "lesser" ] [ text "現に在る夢" ]
    , Html.span [ class "lesser" ] [ text "価値を示せ" ]
    , Html.span [ class "lesser" ] [ text "人は見たいものを見る" ]
    ]



